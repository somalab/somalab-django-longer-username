`somalab-django-longer-username` provides a migration and a monkeypatch to make the django auth.user username field longer, instead of the arbitrarily short 30 characters. 

It's designed to be a simple include-and-forget project that makes a little headache go away.  Enjoy, and pull requests welcome!

Forked from original `django-longer-username`

Credits
=======
Original `django-longer-username`
The monkeypatch for this is very largely based on [celement's answer on stackoverflow](http://stackoverflow.com/questions/2610088/can-djangos-auth-user-username-be-varchar75-how-could-that-be-done)
